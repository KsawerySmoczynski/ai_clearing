from typing import Union, List, Iterator

import numpy as np
import torch as t
from torch import nn
from torch.nn import Parameter
from torch.optim import Optimizer, Adam, SGD

from configs import METRICS
from net.baseline import BaselineNet
from net.mynet import MyNet


def get_samples_weights(*, targets: t.Tensor, indices: Union[t.Tensor, List, np.ndarray]) -> t.Tensor:
    selected_targets = targets[indices] if indices else targets

    class_sample_count = t.tensor([(selected_targets == t).sum() for t in t.unique(targets, sorted=True)])
    weight = 1. / class_sample_count.float()
    samples_weights = t.tensor([weight[t] for t in selected_targets])

    return samples_weights


def is_balanced(targets: Union[t.Tensor, List, np.ndarray]):
    """
    Checks whether the dataset has balanced classes
    :param targets:
    :return: bool
    """
    targets = targets if isinstance(targets, t.Tensor) else t.tensor(targets)

    assert len(targets.shape) == 1, "Targets have to be 1d array"

    counts = t.tensor([(targets == cls).sum() for cls in targets.unique()]).tolist()

    return len(set(counts)) == 1


def train_validation_indices(*, dataset_size, validation_size):
    indices = list(range(dataset_size))
    np.random.shuffle(indices)
    split = int(dataset_size * validation_size)
    train_indices = indices[:-split]
    valid_indices = indices[-split:]

    return train_indices, valid_indices


def get_optimizer(optimizer: str, lr: float, params: Iterator[Parameter]):
    if optimizer == "Adam":
        return Adam(params=params, lr=lr)
    elif optimizer == "SGD":
        return SGD(params=params, lr=lr)
    else:
        print(f"Optimizer unknown, defaulting to {SGD}")
        return SGD(params=params, lr=0.001)


def get_model(*, model: str, l1: int):
    assert isinstance(l1, int) and l1 > 0, "l1 has to be integer >0"

    if model == "MyNet":
        return MyNet(l1=l1)
    elif model == "BaselineNet":
        return BaselineNet(l1=l1)
    else:
        raise ValueError(f"Model {model} currently unsupported")


def generalization_error_normalized_accuracy(train_accuracy: float, test_accuracy: float, eps: float = 1e-5):
    """
    My custom metric related to this task.
    Provides information about the divergence of training and testing performance.
    Puts emphasis on generalization performance.

    Logarithm as monotonically increasing function prevents values from exploding.

    :param train_accuracy: float
    :param test_accuracy: float
    :param eps: default 1e5
    :return: float
    """
    assert train_accuracy >= 0 and test_accuracy >= 0, "Accuracies can only be positive numbers"
    assert eps >= 0, "Epsilon has to be >0"

    return np.log((train_accuracy+test_accuracy) / (abs(train_accuracy-test_accuracy) + eps) + 1)

def get_bool(x:str):
    if x.lower() in ["t", "true", "yes", "y", "1"]:
        return True
    elif x.lower() in ["f","false", "no", "n", "0"]:
        return False
    else:
        raise ValueError(f"Flag {x} unrecognized")

def get_metric(metric:str):
    if metric in METRICS:
        return metric
    else:
        if type(metric) != str:
            raise TypeError(f"Wrong metric {metric} type: {type(metric)}")
        raise ValueError(f"Metric {metric} currently unsupported")
