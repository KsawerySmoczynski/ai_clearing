from typing import List, Tuple

import numpy as np
import torch as t
from torch import nn
from torchvision.datasets import ImageFolder

from utils.utils import get_model


def baseline_oneshot(test_dataset: ImageFolder, train_dataset: ImageFolder) -> Tuple:

    preds = []
    all_distances = []
    for img, target in test_dataset:
        distances = {0: None, 1: None}
        for anchor_img, anchor_target in train_dataset:
            distances[anchor_target] = t.linalg.norm(anchor_img - img).item()
        all_distances.append(distances)
        y_hat = np.argmin([distances[label] for label in range(len(train_dataset.targets))])
        preds.append(y_hat)
    return t.tensor(preds), all_distances


def nn_oneshot(device, test_dataset, train_dataset, training_info) -> Tuple:
    net = get_model(model=training_info["model"], l1=training_info["l1"])
    net.load_state_dict(training_info["model_params"])
    net.to(device)
    net.eval()
    with t.no_grad():
        anchors_precomputed = []
        for anchor_img, anchor_target in train_dataset:
            anchor_img, anchor_target = anchor_img.unsqueeze(0).to(device), t.tensor(anchor_target).to(device)
            anchor_vector = net.oneshot_forward(anchor_img)
            anchors_precomputed.append((anchor_vector, anchor_target))

        preds = []
        all_distances = []
        criterion = nn.CosineSimilarity()
        for img, target in test_dataset:
            img, target = img.unsqueeze(0).to(device), t.tensor(target).to(device)
            img_vector = net.oneshot_forward(img)
            distances = {0: None, 1: None}
            for anchor_vector, anchor_target in anchors_precomputed:
                distances[anchor_target.item()] = criterion(img_vector, anchor_vector).item()
            all_distances.append(distances)
            y_hat = np.argmin([distances[label] for label in range(len(train_dataset.targets))])
            preds.append(y_hat)

    return t.tensor(preds), all_distances
