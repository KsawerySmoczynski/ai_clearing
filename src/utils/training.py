import copy
import os
import sys
from datetime import datetime
from itertools import product
from pathlib import Path
from typing import Dict, Tuple, Union

import torch as t
from torch import nn
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

from configs import METRICS
from utils.utils import get_optimizer, get_model, generalization_error_normalized_accuracy


def train_validate(net: nn.Module, optimizer: t.optim.Optimizer, criterion, metric: str, epochs: int, device: t.device,
                   train_dataloader: DataLoader, test_dataloader: DataLoader, writer: SummaryWriter = False) -> Dict:

    print(f"Running training with net:{net.__class__.__name__}, coding layer output:{net.out_linear.in_features}, "
          f"Optimizer:{optimizer.__class__.__name__}, lr:{optimizer.defaults['lr']}, weight decay:{optimizer.defaults['weight_decay']}")

    n_train = len(train_dataloader.dataset)
    n_test = len(test_dataloader.dataset)

    best_generalization_metric = 0
    best_epoch = None
    net_params = None
    global_train_metrics, global_test_metrics = get_metrics(test=True)

    for ei in range(epochs):
        train_metrics, test_metrics = get_metrics(test=True)
        net.train()
        train_loss = 0
        for idx, (imgs, targets) in enumerate(train_dataloader, 1):
            imgs, targets = imgs.to(device), targets.to(device)
            optimizer.zero_grad()
            y_hat = net(imgs)
            loss = criterion(y_hat, targets.unsqueeze(1).float())
            loss.backward()
            optimizer.step()

            train_loss += loss.item()
            batch_metrics = get_binary_metrics(preds=y_hat, targets=targets)

            train_metrics = update_moving_metrics(global_metrics=train_metrics, batch_metrics=batch_metrics, idx=idx)
            if writer:
                global_step = ei * len(train_dataloader) + idx
                writer.add_scalar(tag="batch_train_loss", scalar_value=loss.item(), global_step=global_step)

        print(f"Train Epoch {ei} loss = {train_loss}")
        print(f"Train Epoch {ei} {metric} = {train_metrics[metric]}")


        with t.no_grad():
            net.eval()
            test_loss = 0
            for idx, (imgs, targets) in enumerate(test_dataloader, 1):
                imgs, targets = imgs.to(device), targets.to(device)
                y_hat = net(imgs)
                test_loss += criterion(y_hat, targets.unsqueeze(1).float()).item()

                batch_metrics = get_binary_metrics(preds=y_hat, targets=targets)
                test_metrics = update_moving_metrics(global_metrics=test_metrics, batch_metrics=batch_metrics, idx=idx)
            print(f"Test Epoch {ei} loss = {test_loss}")
            print(f"Test Epoch {ei} {metric} = {test_metrics[metric]}")

        train_loss /= n_train
        test_loss /= n_test
        generalization_metric = generalization_error_normalized_accuracy(train_metrics["accuracy"], test_metrics["accuracy"])

        if writer:
            writer.add_scalar(tag="epoch_train_loss", scalar_value=train_loss, global_step=ei)
            writer.add_scalar(tag="epoch_test_loss", scalar_value=test_loss, global_step=ei)
            writer.add_scalar(tag="generalization_metric", scalar_value=generalization_metric, global_step=ei)
            writer.add_scalars(main_tag="train_metrics", tag_scalar_dict=train_metrics, global_step=ei)
            writer.add_scalars(main_tag="test_metrics", tag_scalar_dict=test_metrics, global_step=ei)

        # As our network easily fits labels with 99% accuracy,
        # we'll take one with lowest generalization error.
        if compare_metrics(global_test_metrics, test_metrics, metric) and generalization_metric > best_generalization_metric:
            net_params = copy.deepcopy(net.state_dict())
            best_generalization_metric = generalization_metric
            best_epoch = ei + 1
            global_test_metrics = test_metrics
            global_train_metrics = train_metrics

    training_info = {"model_params": net_params,
                     "metrics": {**global_test_metrics,
                                 "generalization_metric": best_generalization_metric},
                     "best_test_metrics": global_test_metrics,
                     "best_train_metrics": global_train_metrics,
                     "epochs": best_epoch
                     }

    return training_info


def train(net: nn.Module, optimizer: t.optim.Optimizer, criterion, metric: str, epochs: int, device: t.device,
          train_dataloader: DataLoader, writer: SummaryWriter = False) -> Dict:
    n_train = len(train_dataloader.dataset)

    best_epoch = None
    net_params = None
    global_train_metrics, _ = get_metrics()

    for ei in range(epochs):
        train_metrics, _ = get_metrics()
        net.train()
        train_loss = 0
        for idx, (imgs, targets) in enumerate(train_dataloader, 1):
            imgs, targets = imgs.to(device), targets.to(device)
            optimizer.zero_grad()
            y_hat = net(imgs)
            loss = criterion(y_hat, targets.unsqueeze(1).float())
            loss.backward()
            optimizer.step()

            batch_metrics = get_binary_metrics(preds=y_hat, targets=targets)
            train_metrics = update_moving_metrics(global_metrics=train_metrics, batch_metrics=batch_metrics, idx=idx)
            train_loss += loss.item()
            if writer:
                global_step = ei * len(train_dataloader) + idx
                writer.add_scalar(tag="batch_train_loss", scalar_value=loss.item(), global_step=global_step)

        train_loss /= n_train

        if writer:
            writer.add_scalar(tag="epoch_train_loss", scalar_value=train_loss, global_step=ei)
            writer.add_scalars(main_tag="metrics", tag_scalar_dict=train_metrics, global_step=ei)

        if compare_metrics(global_train_metrics, train_metrics, metric=metric):
            net_params = copy.deepcopy(net.state_dict())
            best_epoch = ei
            global_train_metrics = train_metrics

    training_info = {"model_params": net_params,
                     "metrics": global_train_metrics,
                     "global_train_metrics": global_train_metrics,
                     "best_test_metrics": None,
                     "epochs": best_epoch
                     }

    return training_info


def hyperparametrize(*, config: dict, train_dataloader: DataLoader, validation_dataloader: DataLoader) -> Dict:
    none_metrics, _ = get_metrics(test=False, default_value=None)
    best_model_info = {"metrics": none_metrics}

    for model, l1 in product(*config["net"].values()):
        for optim, lr in product(*config["optimizer"].values()):
            net = get_model(model=model, l1=l1)
            net.to(config["device"])
            optimizer = get_optimizer(optimizer=optim, lr=lr, params=net.parameters())
            now = datetime.strftime(datetime.now(), "%Y-%m-%d_%H-%M")
            logging_info = f"{model}_{l1}_{optim}_{lr}_{now}"
            if config["logging_path"]:
                writer = SummaryWriter(log_dir=f"{config['logging_path']}/{logging_info}", comment=logging_info)
            training_info = train_validate(net=net, optimizer=optimizer, criterion=config["criterion"],
                                           metric=config["metric"], epochs=config["epochs"], device=config["device"],
                                           train_dataloader=train_dataloader, test_dataloader=validation_dataloader,
                                           writer=writer)
            if config["logging_path"]:
                writer.flush()

            if compare_metrics(old_metrics=best_model_info["metrics"], new_metrics=training_info["metrics"],
                               metric=config["metric"]):
                best_model_info = training_info
                best_model_info["logging_info"] = logging_info
                best_model_info["model"] = model
                best_model_info["l1"] = l1
                best_model_info["optimizer"] = optim
                best_model_info["lr"] = lr

    if config["models_path"]:
        path = f"{config['models_path']}/{best_model_info['logging_info']}".replace(".", ",")
        t.save(obj=best_model_info, f=f"{path}.pt")

    return best_model_info


def compare_metrics(old_metrics: Dict, new_metrics: Dict, metric: str):
    if old_metrics[metric] == None:
        return True

    # Stymulanty i destymulanty
    if metric in ["TP", "TN", "recall", "precision", "f1", "accuracy"]:
        return old_metrics[metric] < new_metrics[metric]
    else:
        return old_metrics[metric] > new_metrics[metric]


def get_binary_metrics(preds: t.Tensor, targets: t.Tensor) -> Dict:
    """
    Function for calculating basic classification metrics
    :param preds: t.Tensor - predictions
    :param targets: t.Tensor - targets
    :return: dictionary containing values for True Positives, True Negative, False Positives, False Negatives, Precision, Recall, Accuracy and F1
    """
    assert preds.numel() == targets.numel(), "Both inputs have to contain same number of values"
    preds = t.round(preds.reshape(targets.shape))

    TP = sum(preds[(targets == preds)] == 1)
    TN = sum(preds[(targets == preds)] == 0)
    FP = sum(preds[(targets != preds)] == 1)
    FN = sum(preds[(targets != preds)] == 0)

    recall = TP / (TP + FN)
    precision = TN / (TN + FP)
    accuracy = sum(preds == targets) / len(preds)
    f1 = 2 * ((precision * recall) / (precision + recall))

    metrics = {"TP": TP,
               "TN": TN,
               "FP": FP,
               "FN": FN,
               "accuracy": accuracy,
               "recall": recall,
               "precision": precision,
               "f1": f1}

    return metrics


def update_moving_metrics(global_metrics: Dict, batch_metrics: Dict, idx: int) -> Dict:
    """
    Function for updating the metrics dictionary

    :param global_metrics: Dict containing metrics
    :param batch_metrics: Dict containing metrics
    :param idx:
    :return: updated global_metrics dict
    """
    assert idx > 0, "Batch idx has to be >0"
    assert set(global_metrics.keys()) == set(batch_metrics.keys()), "Both dictionaries has to contain same keys"

    for metric in ["TP", "TN", "FP", "FN"]:
        global_metrics[metric] += batch_metrics[metric]

    for metric in ["accuracy", "recall", "precision", "f1"]:
        global_metrics[metric] = ((idx-1) / idx) * global_metrics[metric] + (1 / idx) * batch_metrics[metric]

    return global_metrics


def get_metrics(test: bool = False, default_value: Union[float, None] = 0) -> Tuple:
    """
    Returns tuple of dictionaries with zeroed classification metrics
    :param test: bool, if false second output is equal to None
    :return: tuple
    """

    train_metrics = {metric: default_value for metric in METRICS}
    if test:
        test_metrics = dict()
        test_metrics.update(train_metrics)
    else:
        test_metrics = None

    return train_metrics, test_metrics
