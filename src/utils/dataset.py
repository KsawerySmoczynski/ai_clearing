import os
from typing import Optional, Callable
from urllib.error import URLError

import torch as t
from torchvision.datasets import FashionMNIST
from torchvision.datasets.mnist import read_image_file, read_label_file
from torchvision.datasets.utils import download_and_extract_archive


class ShoeDataset(FashionMNIST):
    """
    Class transforming raw ubyte.gz files to pytorch pt format based on the FashionMNIST dataset class with labels alteration.
    """

    def __init__(
            self,
            root: str,
            train: bool = True,
            transform: Optional[Callable] = None,
            target_transform: Optional[Callable] = None,
            download: bool = False,
    ) -> None:
        self.classes = super(ShoeDataset, self).classes
        self.labels_mapper = {idx: self.shoe_mapping(cls) for idx, cls in enumerate(self.classes)}
        self.classes = ["Not-Shoe", "Shoe"]

        super(FashionMNIST, self).__init__(root=root, train=train, transform=transform,
                                           target_transform=target_transform, download=download)

    def shoe_mapping(self, cls: str) -> int:
        return 1 if cls in ['Sandal', 'Sneaker', 'Ankle boot'] else 0

    def download(self) -> None:
        """Download the MNIST data if it doesn't exist in processed_folder already."""

        if self._check_exists():
            return

        os.makedirs(self.raw_folder, exist_ok=True)
        os.makedirs(self.processed_folder, exist_ok=True)

        # download files
        for filename, md5 in self.resources:
            for mirror in self.mirrors:
                url = "{}{}".format(mirror, filename)
                try:
                    print("Downloading {}".format(url))
                    download_and_extract_archive(
                        url, download_root=self.raw_folder,
                        filename=filename,
                        md5=md5
                    )
                except URLError as error:
                    print(
                        "Failed to download (trying next):\n{}".format(error)
                    )
                    continue
                finally:
                    print()
                break
            else:
                raise RuntimeError("Error downloading {}".format(filename))

        # process and save as torch files
        print('Processing...')

        # TODO apply_ is slow, change to tensor operations
        training_set = (
            read_image_file(os.path.join(self.raw_folder, 'train-images-idx3-ubyte')),
            read_label_file(os.path.join(self.raw_folder, 'train-labels-idx1-ubyte')).apply_(
                lambda x: self.labels_mapper[x])
        )
        test_set = (
            read_image_file(os.path.join(self.raw_folder, 't10k-images-idx3-ubyte')),
            read_label_file(os.path.join(self.raw_folder, 't10k-labels-idx1-ubyte')).apply_(
                lambda x: self.labels_mapper[x])
        )
        with open(os.path.join(self.processed_folder, self.training_file), 'wb') as f:
            t.save(training_set, f)
        with open(os.path.join(self.processed_folder, self.test_file), 'wb') as f:
            t.save(test_set, f)

        print('Done!')
