import multiprocessing as mp
import os
from typing import Tuple, Union, List

import numpy as np
import torch as t
from torch.utils.data import Subset, WeightedRandomSampler, DataLoader, Dataset, RandomSampler, ConcatDataset, Sampler
from torchvision import transforms

from utils.dataset import ShoeDataset
from utils.utils import train_validation_indices, get_samples_weights, is_balanced


def load_shoe_data(*, batch_size: int, data_path: str, validation_fraction: float,
                   dataset_transform: transforms.Compose=None, num_workers:int) -> Tuple[dict, dict]:
    # TODO add parameters required for validation
    # TODO add tests
    # if validation exists split accordingly to the validation param
    # else return full datasets

    assert batch_size > 0, "Batch size has to be greater than 0"
    assert os.path.isdir(data_path), "Provide valid path"

    train_dataset = ShoeDataset(root=f"{data_path}/task1", download=True, transform=dataset_transform, train=True)
    test_dataset = ShoeDataset(root=f"{data_path}/task1", download=True, transform=dataset_transform, train=False)

    train_indices, validation_indices = train_validation_indices(dataset_size=len(train_dataset),
                                                                 validation_size=validation_fraction)
    train_sampler = get_sampler(dataset=train_dataset, indices=train_indices)

    validation_dataset = Subset(train_dataset, validation_indices)
    train_dataset = Subset(train_dataset, train_indices)

    train_loader = DataLoader(dataset=train_dataset, sampler=train_sampler, batch_size=batch_size,
                              pin_memory=True, num_workers=num_workers)

    validation_loader = DataLoader(dataset=validation_dataset, batch_size=batch_size, pin_memory=True,
                                   num_workers=num_workers)
    test_loader = DataLoader(dataset=test_dataset, batch_size=batch_size, pin_memory=True, num_workers=num_workers)

    datasets = {"train": train_dataset,
                "validation": validation_dataset,
                "test": test_dataset}

    dataloaders = {"train": train_loader,
                   "validation": validation_loader,
                   "test": test_loader}

    return datasets, dataloaders


def get_sampler(*, dataset: Union[Dataset, ConcatDataset],
                indices: Union[t.Tensor, List, np.ndarray] = False) -> Union[RandomSampler, WeightedRandomSampler]:
    assert isinstance(dataset, Dataset), "dataset has subclass torch.utils.data.Dataset"
    if indices:
        assert (isinstance(indices, (t.Tensor, list, np.ndarray)) and len(t.tensor(indices).shape) == 1), "indices have to be 1d array of type list, np.ndarray or torch.Tensor"

    train_dataset = Subset(dataset=dataset, indices=indices) if indices else dataset

    targets = get_targets(dataset=dataset)

    if is_balanced(targets=targets):
        sampler = RandomSampler(data_source=train_dataset)
    else:
        samples_weights = get_samples_weights(targets=targets, indices=indices)
        sampler = WeightedRandomSampler(weights=samples_weights, num_samples=len(samples_weights))

    return sampler

def get_targets(dataset:Union[ConcatDataset, Subset, Dataset]) -> t.Tensor:
    assert isinstance(dataset, Dataset), f"dataset has to subclass {Dataset}"

    if isinstance(dataset, ConcatDataset):
        targets = t.cat(tensors=list(set([get_targets(data) for data in dataset.datasets])))
    elif isinstance(dataset, Subset):
        targets = dataset.dataset.targets[dataset.indices]
    else:
        targets = dataset.targets

    return targets
