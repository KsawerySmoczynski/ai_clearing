from datetime import datetime
from pathlib import Path
from typing import Dict

import random
import numpy as np
import torch as t
from torch import nn
from torch.optim import Adam
from torch.utils.data import DataLoader, ConcatDataset
from torchvision import transforms
from torch.utils.tensorboard import SummaryWriter
from torchvision.datasets import ImageFolder

from configs import OPTIMIZER_SEARCH_SPACE, NET_SEARCH_SPACE
from net.baseline import BaselineNet
from utils.utils import get_model, get_optimizer
from utils.training import train, train_validate, hyperparametrize, get_binary_metrics
from utils.data import load_shoe_data, get_sampler
from utils.training_oneshot import baseline_oneshot, nn_oneshot


def task1(*, data_path: str, models_path: str, logging_path: str, writer: bool,
          full_train: bool, epochs: int, validation_fraction: float, batch_size: int, metric: str,
          device: str, num_workers: int, verbose: bool, **kwargs) -> dict:
    """
    Function running the binary classification task on the FashionMNIST dataset,
    returning dictionary of paths for model and other required entities for task2

    :return: dict
    """

    vprint = lambda x: print(f"Task1: {x}") if verbose else lambda x: None
    device = t.device(device)

    # Loading data
    dataset_transform = transforms.Compose([transforms.ToTensor(),
                                            transforms.Normalize((.5,), (.5,)),
                                            transforms.RandomHorizontalFlip(.5)])

    datasets, dataloaders = load_shoe_data(batch_size=batch_size, data_path=data_path,
                                           validation_fraction=validation_fraction, dataset_transform=dataset_transform,
                                           num_workers=num_workers)

    config = {
        "net": NET_SEARCH_SPACE,
        "optimizer": OPTIMIZER_SEARCH_SPACE,
        "criterion": nn.BCELoss(),
        "metric": metric,
        "epochs": epochs,
        "device": device,
        "models_path": f"{models_path}/task1" if models_path else False,
        "logging_path": f"{logging_path}/task1" if writer else False
    }

    if models_path:
        Path(config["models_path"]).mkdir(parents=True, exist_ok=True)
    if logging_path:
        Path(config["logging_path"]).mkdir(parents=True, exist_ok=True)

    # Baseline model:
    net = BaselineNet(l1=16)
    net.to(device)
    optimizer = Adam(params=net.parameters(), lr=.001)
    vprint("Running baseline model:")
    training_info = train_validate(net=net, optimizer=optimizer, criterion=config["criterion"], metric=config["metric"],
                                   epochs=config["epochs"], device=device, train_dataloader=dataloaders["train"],
                                   test_dataloader=dataloaders["validation"], writer=False)
    vprint(f"Baseline model scored {training_info['metrics'][metric]} best {metric} on validation set.")

    # Hyperparametrization
    vprint(f"Running hyperparametrization")
    net_info_dict = hyperparametrize(config=config, train_dataloader=dataloaders["train"],
                                     validation_dataloader=dataloaders["test"])
    vprint(f"Chosen model: {net_info_dict['model']}, chosen l1: {net_info_dict['l1']}")
    vprint(f"Chosen optimizer:{net_info_dict['optimizer']}, with learning rate: {net_info_dict['lr']}")
    vprint(
        f"Scored {training_info['metrics'][metric]} {metric} on test set with generalization metric = {training_info['metrics']['generalization_metric']}")

    # Obtaining dataset for next phase training
    if full_train:
        full_train_dataset = ConcatDataset(datasets=[datasets["train"], datasets["validation"]])
    else:
        full_train_dataset = datasets["validation"]
    full_train_sampler = get_sampler(dataset=full_train_dataset)
    full_train_loader = DataLoader(dataset=full_train_dataset, batch_size=batch_size, sampler=full_train_sampler,
                                   num_workers=num_workers)
    # Getting best model configs
    net = get_model(model=net_info_dict["model"], l1=net_info_dict["l1"])
    if not full_train:
        net.load_state_dict(net_info_dict["model_params"])
    net.to(device)
    optimizer = get_optimizer(optimizer=net_info_dict["optimizer"], lr=net_info_dict["lr"], params=net.parameters())

    # Training on full train set and evaluating on test set
    if writer:
        now = datetime.strftime(datetime.now(), "%Y-%m-%d_%H-%M")
        summary_writer = SummaryWriter(log_dir=f"{logging_path}/task1/evaluation_training_{now}")
    else:
        summary_writer = writer

    vprint(f"Running evaluation")
    training_info = train_validate(net=net, optimizer=optimizer, criterion=config["criterion"], metric=config["metric"],
                                   epochs=net_info_dict["epochs"], device=device, train_dataloader=full_train_loader,
                                   test_dataloader=dataloaders["test"], writer=summary_writer)

    vprint(
        f"Task1: Scored {training_info['metrics'][metric]} {metric} on test set with generalization metric = {training_info['metrics']['generalization_metric']}")

    # Final training
    if full_train:
        whole_dataset = ConcatDataset(datasets=datasets.values())
        final_epochs = net_info_dict["epochs"]
    else:
        whole_dataset = datasets["test"]
        final_epochs = 2

    whole_dataset_sampler = get_sampler(dataset=whole_dataset)
    whole_dataloader = DataLoader(dataset=whole_dataset, batch_size=batch_size, sampler=whole_dataset_sampler,
                                  num_workers=num_workers)

    net = get_model(model=net_info_dict["model"], l1=net_info_dict["l1"])
    if not full_train:
        net.load_state_dict(net_info_dict["model_params"])
    net.to(device)
    optimizer = get_optimizer(optimizer=net_info_dict["optimizer"], lr=net_info_dict["lr"], params=net.parameters())

    vprint("Training on all samples dataset")
    training_info = train(net=net, optimizer=optimizer, criterion=config["criterion"], metric=config["metric"],
                          epochs=final_epochs, device=device, train_dataloader=whole_dataloader)

    if models_path:
        now = datetime.strftime(datetime.now(), "%Y-%m-%d_%H-%M")
        path = f"{config['models_path']}/final_training_{now}".replace(".", ",")
        t.save(obj=training_info, f=f"{path}.pt")

    net_info_dict.update(training_info)
    training_info = net_info_dict

    return training_info


def task2(*, training_info: Dict, data_path: str, device: str, metric: str, verbose: bool, **kwargs):
    vprint = lambda x: print(f"Task 2: {x}") if verbose else lambda x: None

    data_path = f"{data_path}/task2"
    Path(data_path).mkdir(parents=True, exist_ok=True)
    device = t.device(device)

    dataset_transform = transforms.Compose([transforms.Grayscale(num_output_channels=1),
                                            transforms.ToTensor(),
                                            transforms.Normalize((.5,), (.5,))]
                                           )

    train_dataset = ImageFolder(root=f"{data_path}/train", transform=dataset_transform)
    test_dataset = ImageFolder(root=f"{data_path}/test", transform=dataset_transform)

    # Naive model should score 50%
    vprint(f"Naive model (random guess) scores 50%")
    # Baseline model knn
    y_hat_baseline, baseline_distances = baseline_oneshot(test_dataset, train_dataset)
    baseline_metrics = get_binary_metrics(preds=y_hat_baseline.float(), targets=t.tensor(test_dataset.targets))
    vprint(f"Baseline model (euclidean distance) scores {baseline_metrics[metric]} {metric}")

    y_hat_nn, distances = nn_oneshot(device, test_dataset, train_dataset, training_info)
    nn_metrics = get_binary_metrics(preds=y_hat_nn.float(), targets=t.tensor(test_dataset.targets))
    vprint(f"NN model scores {nn_metrics[metric]} {metric}")
    if nn_metrics["accuracy"] < .5:
        vprint(":(")


if __name__ == "__main__":
    pass
