import unittest
from builtins import TypeError

import numpy as np
import torch as t

from configs import METRICS
from utils import utils

verbose = False

vprint = print if verbose else lambda x: None


class GetModelTest(unittest.TestCase):
    @classmethod
    def setUpClass(self) -> None:
        self.l1_correct = 128
        self.l1_wrong = -128
        self.mynet = "MyNet"
        self.baselinenet = "BaselineNet"
        self.other = ["ResNet"]

    @classmethod
    def tearDownClass(self) -> None:
        pass

    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def test_correct_model(self):
        self.assertTrue(type(utils.get_model(model=self.mynet, l1=self.l1_correct)), )


class Generalization_error_normalized_accuracyTest(unittest.TestCase):
    @classmethod
    def setUpClass(self) -> None:
        self.negative_value = -.5
        self.positive_value = .5
        self.eps = 1e-5

    @classmethod
    def tearDownClass(self) -> None:
        pass

    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def test_correct_metric(self):
        f = utils.generalization_error_normalized_accuracy
        self.assertGreaterEqual(f(self.positive_value, self.positive_value, self.eps), 0)
        self.assertGreaterEqual(f(0, 0), 0)
        self.assertGreaterEqual(f(t.tensor(0), t.tensor(1)), 0)

    def test_assertion_error(self):
        f = utils.generalization_error_normalized_accuracy
        with self.assertRaises(AssertionError):
            f(self.negative_value, self.positive_value, self.eps)
        with self.assertRaises(AssertionError):
            f(self.positive_value, self.negative_value)
        with self.assertRaises(AssertionError):
            f(self.negative_value, self.negative_value)


class GetBoolTest(unittest.TestCase):
    @classmethod
    def setUpClass(self) -> None:
        self.true = ["t", "true", "yes", "y", "1"]
        self.false = ["f", "false", "no", "n", "0"]

    @classmethod
    def tearDownClass(self) -> None:
        pass

    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def test_get_bool_correct(self):
        for flag in self.true:
            self.assertTrue(utils.get_bool(flag))
        for flag in self.false:
            self.assertTrue(not utils.get_bool(flag))

    def test_get_bool_wrong_value(self):
        checks = [" ", "lala"]
        for check in checks:
            with self.assertRaises(ValueError):
                utils.get_bool(check)

    def test_get_bool_type_error(self):
        checks = [1, dict(), ["hey"]]
        for check in checks:
            with self.assertRaises(AttributeError):
                utils.get_bool(check)


class GetMetricTest(unittest.TestCase):

    @classmethod
    def setUpClass(self) -> None:
        self.valid_metrics = METRICS

    @classmethod
    def tearDownClass(self) -> None:
        pass

    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def test_get_metric_supported(self):
        print(self.valid_metrics)
        for metric in self.valid_metrics:
            self.assertEqual(utils.get_metric(metric), metric)

    def test_get_metric_unsupported(self):
        metrics_unsupported = set(["AUC", "ROC", "RMSE", "SMAPE"]).difference(self.valid_metrics)
        for metric_unsupported in metrics_unsupported:
            with self.assertRaises(ValueError):
                utils.get_metric(metric_unsupported)

    def test_get_metric_wrong_type(self):
        metrics = [{"A": "a"}, ["b"]]
        for metric in metrics:
            with self.assertRaises(TypeError):
                utils.get_metric(metric)


if __name__ == '__main__':
    unittest.main()
