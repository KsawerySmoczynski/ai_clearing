import os
import shutil
import unittest

import torch as t

from utils.dataset import ShoeDataset
from configs import DATA_PATH


class ShoeDatasetTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("Do this before everything")
        train_dataset = ShoeDataset(DATA_PATH, download=True)
        train_dataset = ShoeDataset(DATA_PATH, download=True)

    @classmethod
    def tearDownClass(cls) -> None:
        # shutil.rmtree(f"{DATA_PATH}/ShoeDataset")
        print("Tearing down")

    def setUp(self) -> None:
        print("Set up before each test")

    def tearDown(self) -> None:
        print("Tear down after each test")

    def test_download(self):
        print("Testing download method")
        folder_exists = os.path.isdir(f"{DATA_PATH}/ShoeDataset")
        self.assertTrue(folder_exists, "Files were not downloaded")

        files = os.listdir(f"{DATA_PATH}/ShoeDataset/processed")
        self.assertIn("test.pt", files, "No processed testfile")
        self.assertIn("training.pt", files, "No processed testfile")

        test_file = f"{DATA_PATH}/ShoeDataset/processed/test.pt"
        train_file = f"{DATA_PATH}/ShoeDataset/processed/training.pt"

        if os.path.exists(test_file):
            test_imgs, test_labels = t.load(test_file)
            test_labels_unique = set(test_labels.unique().tolist())
            print(f"test_labels_unique={test_labels_unique}")
            self.assertEqual(test_labels_unique, {0, 1}, "Test labels don't match")
        if os.path.exists(train_file):
            test_imgs, test_labels = t.load(train_file)
            self.assertEqual(set(test_labels.unique().tolist()), {0, 1}, "Train labels don't match")


if __name__ == '__main__':
    unittest.main()
