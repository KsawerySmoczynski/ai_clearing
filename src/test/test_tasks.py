import unittest
import tasks
import configs


class MyTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("Do this before everything")

    @classmethod
    def tearDownClass(cls) -> None:
        print("Do this after everything")

    def setUp(self) -> None:
        print("Set up before each test")

    def tearDown(self) -> None:
        print("Tear down after each test")

    def test_task1(self):
        self.assertEqual(sum([1,2,3]), 6, "Should be 6")
        # self.assertIsInstance(tasks.task1(data_path=configs.DATA_PATH), dict)


if __name__ == '__main__':
    unittest.main()
