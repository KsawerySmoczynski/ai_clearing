import random
import unittest

import torch as t
from torch.nn import Parameter
from torch.optim import Adam

from net.baseline import BaselineNet
from net.mynet import MyNet
from utils import utils
from utils.utils import get_model, get_optimizer

verbose = False

vprint = print if verbose else lambda x: None

class UtilsModuleTest(unittest.TestCase):

    @classmethod
    def setUpClass(self) -> None:
        self.one = 1
        vprint("Do this before everything")

    @classmethod
    def tearDownClass(self) -> None:
        vprint("Do this after everything")

    def setUp(self) -> None:
        vprint("Set up before each test")

    def tearDown(self) -> None:
        vprint("Tear down after each test")

    def test_get_model(self):
        # Good cases
        model = "MyNet"
        l1 = random.randint(0, 1000)
        device = t.device("cpu")

        self.assertIsInstance(get_model(model=model, l1=l1), MyNet)
        if t.cuda.is_available():
            device = t.device("cuda")
            self.assertIsInstance(get_model(model=model, l1=l1), MyNet)

        # Edge cases
        model = "  "
        self.assertIsInstance(get_model(model=model, l1=l1), BaselineNet)

        with self.assertRaises(AssertionError) as ctx:
            get_model(model=model, l1=-200)

        with self.assertRaises(AssertionError) as ctx:
            get_model(model=model, l1="lol")

    def test_get_optimizer(self):
        # Good cases
        optimizer = "Adam"
        lr = .001
        parameters = iter([Parameter(t.tensor([1,2,3]))])

        self.assertIsInstance(get_optimizer(optimizer=model, lr=lr, parameters=parameters), Adam)

        # Edge cases
        optimizer = "  "
        self.assertIsInstance(get_model(model=model, l1=l1), BaselineNet)

        with self.assertRaises(AssertionError) as ctx:
            get_model(model=model, l1=-200)

        with self.assertRaises(AssertionError) as ctx:
            get_model(model=model, l1="lol")

    def test_matching_count(self):
        t1 = t.tensor([1,2,3]).float()
        t2 = t.tensor([1, 2, 3]).float()
        t3 = t.tensor([1, 2]).float()
        self.assertEqual(utils.matching_count(t1, t2), len(t1), "Should be equal")
        with self.assertRaises(AssertionError) as ctx:
            utils.matching_count(t1.tolist(), t2)

        with self.assertRaises(AssertionError) as ctx:
            utils.matching_count(t1, t3)

if __name__ == '__main__':
    unittest.main()
