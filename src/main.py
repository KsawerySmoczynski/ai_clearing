import argparse
from random import random

import numpy as np
import torch as t

from configs import DEVICE, MODELS_PATH, DATA_PATH, NUM_WORKERS, LOGGING_PATH, VERBOSE, SEED, BATCH_SIZE, EPOCHS, \
    VALID_SIZE, METRIC, WRITER, FULL_TRAIN, METRICS
from tasks import task1, task2
from utils.utils import get_bool, get_metric

def main() -> None:
    parser = argparse.ArgumentParser()
    # Configs
    parser.add_argument("--device", dest="device", type=str, default=DEVICE,
                        help="Device on which training ought to be performed, choose: cpu or cuda.\nScript doesn't support multigpu setup")
    parser.add_argument("--num-workers", dest="num_workers", type=int, default=NUM_WORKERS,
                        help="Number of processess used by the DataLoader instances, defaulting to number of cores.")
    parser.add_argument("--verbose", dest="verbose", type=get_bool, default=VERBOSE,
                        help=f"Run script with verbose or without, bool value")
    parser.add_argument("--seed", dest="seed", type=int, default=SEED)

    #Logging
    parser.add_argument("--data-path", dest="data_path", type=str, default=DATA_PATH,
                        help=f"Path to the training data, defaulting to path from setup.sh script: {DATA_PATH}")
    parser.add_argument("--models-path", dest="models_path", type=str, default=MODELS_PATH,
                        help=f"Path for saving models, defaulting to {MODELS_PATH}")
    parser.add_argument("--writer", dest="writer", type=get_bool, default=WRITER,
                        help=f"To use or not to use Summary writer, that is the question? default:{WRITER}")
    parser.add_argument("--logging-path", dest="logging_path", type=str, default=LOGGING_PATH,
                        help=f"Path for saving logs for tensorboard, use with writer flag. Defaulting to {LOGGING_PATH}")

    #Learning configs
    parser.add_argument("--batch-size", dest="batch_size", type=int, default=BATCH_SIZE)
    parser.add_argument("-e","--epochs", dest="epochs", type=int, default=EPOCHS)
    parser.add_argument("--validation-fraction", dest="validation_fraction", type=float, default=VALID_SIZE)
    parser.add_argument("--metric", dest="metric", type=get_metric, default=METRIC,
                        help=f"Target metric to optimize, currently supported metrics: {', '.join(METRICS)}")
    parser.add_argument("--full-train", dest="full_train", type=get_bool, default=FULL_TRAIN,
                        help=f"Train from scratch (True) or use pretrained network (False) after hyperparametrization, default:{FULL_TRAIN}")

    args = parser.parse_args()

    np.random.seed(args.seed)
    t.manual_seed(args.seed)
    t.cuda.manual_seed(args.seed)
    t.cuda.manual_seed_all(args.seed)
    vprint = print if args.verbose else lambda x: None

    training_info = task1(**vars(args))
    vprint("Task 1 done")
    task2(training_info=training_info, **vars(args))
    vprint("Task 2 done, bye")


if __name__ == "__main__":
    main()
