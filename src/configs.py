import os
import multiprocessing as mp
import torch as t

DATA_PATH = os.path.abspath('../data')
SEED = 42
VERBOSE = True
DEVICE = "cuda" if t.cuda.is_available() else "cpu"
NUM_WORKERS = mp.cpu_count()

# Learning configs
BATCH_SIZE = 128
EPOCHS = 5
VALID_SIZE = .2
METRIC = "accuracy"

# Logging
WRITER = True
LOGGING_PATH = os.path.abspath("../logs")
MODELS_PATH = os.path.abspath("../models")
FULL_TRAIN = False
NET_SEARCH_SPACE = {"model": ["MyNet"],
                    "l1": [256, 128, 64, 32]}

OPTIMIZER_SEARCH_SPACE = {"name": ["SGD","Adam"],
                          "lr": [.003, .001, .0005]}

#Defaults
_stymulanty = ['TP', 'TN', 'accuracy', 'recall', 'precision', 'f1']
_destymulanty = ["FP", "FN"]
METRICS = _stymulanty + _destymulanty
