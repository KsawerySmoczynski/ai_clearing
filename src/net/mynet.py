import torch as t
from torch import nn

class MyNet(nn.Module):
    def __init__(self, l1: int):
        super(MyNet, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=16, kernel_size=5,
                      stride=2, padding=2),
            nn.ReLU(),
            nn.Dropout(.2),
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5,
                      stride=2),
            nn.ReLU(),
            nn.Dropout(.2),
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3,
                      stride=2),
            nn.ReLU(),
            nn.Dropout(.2),
            nn.Conv2d(in_channels=64, out_channels=256, kernel_size=2,
                      stride=1),
            nn.ReLU()
        )

        self.linear = nn.Sequential(
            nn.Linear(in_features=256, out_features=l1),
            nn.Sigmoid()
        )
        self.dropout = nn.Dropout(.5)
        self.out_linear = nn.Linear(in_features=l1, out_features=1)
        self.out_activation = nn.Sigmoid()

    def __call__(self, x: t.Tensor) -> t.Tensor:
        return self.forward(x)

    def oneshot_forward(self, x: t.Tensor) -> t.Tensor:
        x = self.conv(x)
        x = x.view(x.shape[0], -1)
        x = self.linear(x)

        return x

    def forward(self, x: t.Tensor) -> t.Tensor:
        x = self.conv(x)
        x = x.view(x.shape[0], -1)
        x = self.linear(x)
        x = self.out_activation(self.out_linear(self.dropout(x)))

        return x


if __name__ == "__main__":
    net = MyNet(l1=256)

    x = t.randn((128, 1, 28, 28))

    out = net(x)
    out.shape
    out.view(out.shape[0], -1).shape

    out = net.conv(x)
    out.shape
    out.view(out.shape[0], -1).shape

    out = net.conv[1](net.conv[0](x))
    out.shape
    out.view(out.shape[0], -1).shape

    out = net.conv[4](net.conv[3](net.conv[2](out)))
    out.shape
    out.view(out.shape[0], -1).shape

    out = net.conv[9](net.conv[8](net.conv[7](out)))
    out.shape
    out.view(out.shape[0], -1).shape

    out = net.conv[11](net.conv[10](out))
    out.shape

    t.nn.utils.parameters_to_vector(net.parameters()).shape
