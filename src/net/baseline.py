import torch as t
from torch import nn
from torch import functional as F

class DummyModel(nn.Module):
    def __init__(self, l1:int):
        super(DummyModel, self).__init__()

    def __call__(self, x:t.Tensor):
        self.forward(x)

    def forward(self):
        return t.randint(0, 1, size=(x.shape[0], 1))

class BaselineLeNet(nn.Module):

    def __init__(self, l1:int=84):
        super(BaselineLeNet, self).__init__()

        #Changing padding to 2 as images orginally were padded to fit shape 32x32
        self.conv1 = nn.Conv2d(in_channels=1, out_channels=16, kernel_size=5,
                               stride=1, padding=2)
        self.conv2 = nn.Conv2d(in_channels=6, out_channels=16, kernel_size=5,
                               stride=1, padding=0)
        self.conv3 = nn.Conv2d(in_channels=16, out_channels=120, kernel_size=5,
                               stride=1, padding=0)
        self.linear1 = nn.Linear(120, l1)
        self.linear2 = nn.Linear(l1, 1)

        self.tanh = nn.Tanh()
        self.avgpool = nn.AvgPool2d(kernel_size=2, stride=2)
        self.out_activation = nn.Sigmoid()

    def forward(self, x:t.Tensor) -> t.Tensor:
        x = self.avgpool(self.tanh(self.conv1(x)))
        x = self.avgpool(self.tanh(self.conv2(x)))
        x = self.tanh(self.conv3(x))

        x = self.tanh(self.linear1(x.view(x.shape[0], -1)))
        x = self.out_activation(self.linear2(x))

        return x

    def __call__(self, x:t.Tensor):
        return self.forward(x)

    @property
    def params_count(self) -> int:
        return sum([len(param.flatten()) for param in self.parameters()])

class BaselineNet(nn.Module):
    def __init__(self, l1:int):
        super(BaselineNet, self).__init__()

        self.conv = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=16, kernel_size=5,
                      stride=3, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(2,2),
            nn.Conv2d(in_channels=16, out_channels=64, kernel_size=5,
                      stride=1),
            nn.ReLU()
        )

        self.linear = nn.Sequential(
            nn.Linear(64, l1),
            nn.Sigmoid()
        )
        self.out_linear = nn.Linear(l1, 1)
        self.out_activation = nn.Sigmoid()

    def oneshot_forward(self, x:t.Tensor) -> t.Tensor:
        x = self.conv(x)
        x = x.view(x.shape[0], -1)
        x = self.linear(x)

    def forward(self, x:t.Tensor) -> t.Tensor:
        x = self.conv(x)
        x = x.view(x.shape[0], -1)
        x = self.linear(x)
        x = self.out_activation(self.out_linear(x))

        return x

    def __call__(self, x:t.Tensor):
        return self.forward(x)

if __name__ == "__main__":
    net = BaselineNet(l1=128)

    x = t.randn((128,1,28,28))
    out = net(x)
    out.shape
    out.view(out.shape[0], -1).shape

    t.nn.utils.parameters_to_vector(net.parameters()).shape
