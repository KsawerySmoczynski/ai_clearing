# Sources


## Small datasets
* [Paper 1](https://hpi.de/fileadmin/user_upload/fachgebiete/meinel/papers/Web_3.0/ICIP2016-hentschel.pdf)
* [Medium 1](https://towardsdatascience.com/how-to-use-deep-learning-even-with-small-data-e7f34b673987)
* [Medium 2](https://towardsdatascience.com/breaking-the-curse-of-small-data-sets-in-machine-learning-part-2-894aa45277f4)
* [Very relevant](https://arxiv.org/pdf/1901.09054.pdf)
* [dk](https://openreview.net/pdf?id=ghjxvfgv9ht)
* [dk2](https://www.sciencedirect.com/science/article/pii/S0264127518308682)
* [VGG?](https://github.com/ubik3000/small-dataset-transfer-learning)
* [Computational efficiency](https://arxiv.org/pdf/1410.1141.pdf)
* [A Close Look at Deep Learning with Small Data](https://arxiv.org/pdf/2003.12843.pdf)


## One-shot
* [Medium intro](https://connorshorten300.medium.com/one-shot-learning-70bd78da4120)
* [Additional](https://en.wikipedia.org/wiki/One-shot_learning)
* [One shot learning introduction](https://blog.floydhub.com/n-shot-learning/)
* [papers with code](https://paperswithcode.com/task/one-shot-learning)


* [Learning to Compare: Relation Network for Few-Shot Learning 2018](https://arxiv.org/pdf/1711.06025v2.pdf)
* [Learning Deep Representations of Fine-Grained Visual Descriptions](https://arxiv.org/pdf/1605.05395v1.pdf)
* [IMPROVING ZERO-SHOT LEARNING BY MITIGATING
THE HUBNESS PROBLEM](https://arxiv.org/pdf/1412.6568v3.pdf)


## Augmentation
* [ag1](https://nanonets.com/blog/data-augmentation-how-to-use-deep-learning-when-you-have-limited-data-part-2/)



## Todos

* ~~lala~~

### Task 1
* Learn about unittests
* Start Next lesson from introduction to pytorch
* Create basic pipeline for task1

### Task 2
* Learn about siamese net
    ** Triplet loss -> new dataset with different mapping? (Heels, Flat, Not)
    ** Obtain more samples of each class by augmenting the data?
    ** Positive and negative
* Try to apply to second task

### Afterworks
* Improve the model
