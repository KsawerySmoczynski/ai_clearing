#!/bin/bash
#https://medium.com/@codingmaths/bin-bash-what-exactly-is-this-95fc8db817bf

##### Data download #####
mkdir -p data/{task1,task2}
mkdir -p models/{task1,task2} logs/{task1,task2}

#Task 2
wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1_nzKoVlyls51OrveNx0hkorQUieUS4aA' -O data/task2/shoes.zip
unzip data/task2/shoes.zip 'Shoes/*' -d 'data/task2'
mv data/task2/Shoes/test data/task2/test
mv data/task2/Shoes/train data/task2/train
rm -r data/task2/shoes.zip data/task2/Shoes

